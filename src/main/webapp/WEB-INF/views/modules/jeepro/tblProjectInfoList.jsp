<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>项目一对多管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/jeepro/tblProjectInfo/">项目一对多列表</a></li>
		<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><li><a href="${ctx}/jeepro/tblProjectInfo/form">项目一对多添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="tblProjectInfo" action="${ctx}/jeepro/tblProjectInfo/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>ID：</label>
				<form:input path="id" htmlEscape="false" maxlength="11" class="input-medium"/>
			</li>
			<li><label>企业名称：</label>
				<form:input path="enterpriseName" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>企业简称：</label>
				<form:input path="enterpriseAbbreviation" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>项目种类[1-储备项目，2-已投项目]：</label>
				<form:input path="projectType" htmlEscape="false" maxlength="11" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>ID</th>
				<th>企业名称</th>
				<th>企业简称</th>
				<th>项目种类[1-储备项目，2-已投项目]</th>
				<th>备注信息</th>
				<th>最后更新时间</th>
				<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="tblProjectInfo">
			<tr>
				<td><a href="${ctx}/jeepro/tblProjectInfo/form?id=${tblProjectInfo.id}">
					${tblProjectInfo.id}
				</a></td>
				<td>
					${tblProjectInfo.enterpriseName}
				</td>
				<td>
					${tblProjectInfo.enterpriseAbbreviation}
				</td>
				<td>
					${tblProjectInfo.projectType}
				</td>
				<td>
					${tblProjectInfo.remarks}
				</td>
				<td>
					<fmt:formatDate value="${tblProjectInfo.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><td>
    				<a href="${ctx}/jeepro/tblProjectInfo/form?id=${tblProjectInfo.id}">修改</a>
					<a href="${ctx}/jeepro/tblProjectInfo/delete?id=${tblProjectInfo.id}" onclick="return confirmx('确认要删除该项目一对多吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>