/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jeepro.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.jeepro.entity.TblProjectInfo;

/**
 * 项目一对多DAO接口
 * @author xuezp
 * @version 2017-08-16
 */
@MyBatisDao
public interface TblProjectInfoDao extends CrudDao<TblProjectInfo> {
	
}