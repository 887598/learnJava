/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jeepro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.jeepro.entity.TblProjectInfo;
import com.thinkgem.jeesite.modules.jeepro.dao.TblProjectInfoDao;
import com.thinkgem.jeesite.modules.jeepro.entity.TblProjectInvestInfo;
import com.thinkgem.jeesite.modules.jeepro.dao.TblProjectInvestInfoDao;

/**
 * 项目一对多Service
 * @author xuezp
 * @version 2017-08-16
 */
@Service
@Transactional(readOnly = true)
public class TblProjectInfoService extends CrudService<TblProjectInfoDao, TblProjectInfo> {

	@Autowired
	private TblProjectInvestInfoDao tblProjectInvestInfoDao;
	
	public TblProjectInfo get(String id) {
		TblProjectInfo tblProjectInfo = super.get(id);
		tblProjectInfo.setTblProjectInvestInfoList(tblProjectInvestInfoDao.findList(new TblProjectInvestInfo(tblProjectInfo)));
		return tblProjectInfo;
	}
	
	public List<TblProjectInfo> findList(TblProjectInfo tblProjectInfo) {
		return super.findList(tblProjectInfo);
	}
	
	public Page<TblProjectInfo> findPage(Page<TblProjectInfo> page, TblProjectInfo tblProjectInfo) {
		return super.findPage(page, tblProjectInfo);
	}
	
	@Transactional(readOnly = false)
	public void save(TblProjectInfo tblProjectInfo) {
		super.save(tblProjectInfo);
		for (TblProjectInvestInfo tblProjectInvestInfo : tblProjectInfo.getTblProjectInvestInfoList()){
			if (tblProjectInvestInfo.getId() == null){
				continue;
			}
			if (TblProjectInvestInfo.DEL_FLAG_NORMAL.equals(tblProjectInvestInfo.getDelFlag())){
				if (StringUtils.isBlank(tblProjectInvestInfo.getId())){
					tblProjectInvestInfo.setProjectInfoId(tblProjectInfo);
					tblProjectInvestInfo.preInsert();
					tblProjectInvestInfoDao.insert(tblProjectInvestInfo);
				}else{
					tblProjectInvestInfo.preUpdate();
					tblProjectInvestInfoDao.update(tblProjectInvestInfo);
				}
			}else{
				tblProjectInvestInfoDao.delete(tblProjectInvestInfo);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(TblProjectInfo tblProjectInfo) {
		super.delete(tblProjectInfo);
		tblProjectInvestInfoDao.delete(new TblProjectInvestInfo(tblProjectInfo));
	}
	
}