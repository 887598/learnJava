/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jeepro.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 项目一对多Entity
 * @author xuezp
 * @version 2017-08-16
 */
public class TblProjectInfo extends DataEntity<TblProjectInfo> {
	
	private static final long serialVersionUID = 1L;
	private String enterpriseName;		// 企业名称
	private String enterpriseAbbreviation;		// 企业简称
	private String projectType;		// 项目种类[1-储备项目，2-已投项目]
	private String projectSource;		// 项目来源[1-产业链，2-同行，3-已投项目介绍，4-政府/招商，5-券商/律所/会所，6-FA，7-主动发掘，8-其他]
	private String projectStage;		// 发展阶段[1-天使，2-Pre-A，3-A，4-B，5-Pre-IPO]
	private String province;		// 省
	private String city;		// 市
	private String county;		// 区/县
	private String address;		// 地点
	private String projectIntroduction;		// 项目简介
	private String comment;		// 备注
	private String reserveInfoPfa;		// 储备项目信息-拟融资金额
	private String reserveInfoPfv;		// 储备项目信息-拟融资估值
	private String investedInfoProjectNo;		// 已投项目信息-项目编号
	private String investedInfoLegalRepresentative;		// 已投项目信息-法人代表
	private Date investedInfoFoundingTime;		// 已投项目信息-成立时间
	private String investedInfoRegisteredCapital;		// 已投项目信息-注册资本
	private String investedInfoUniformCreditCode;		// 已投项目信息-统一信用代码
	private String investedInfoAccountName;		// 已投项目信息-银行信息-户名
	private String investedInfoBankName;		// 已投项目信息-银行信息-开户行
	private String investedInfoAccountNo;		// 已投项目信息-银行信息-账号
	private String investedInfoComment;		// 已投项目信息-银行信息-备注
	private String qrCodeFile;		// 二维码
	private String projectPlanFile;		// 项目计划书
	private String secrecyFile;		// 保密协议/TS
	private String logoFile;		// Logo
	private String businessLicenseFile;		// 营业执照
	private String industryComment;		// 备注（行业分类为9-其他时的备注）
	private String versionNo;		// 数据版本号[默认为1]
	private List<TblProjectInvestInfo> tblProjectInvestInfoList = Lists.newArrayList();		// 子表列表
	
	public TblProjectInfo() {
		super();
	}

	public TblProjectInfo(String id){
		super(id);
	}

	@Length(min=1, max=50, message="企业名称长度必须介于 1 和 50 之间")
	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	
	@Length(min=1, max=50, message="企业简称长度必须介于 1 和 50 之间")
	public String getEnterpriseAbbreviation() {
		return enterpriseAbbreviation;
	}

	public void setEnterpriseAbbreviation(String enterpriseAbbreviation) {
		this.enterpriseAbbreviation = enterpriseAbbreviation;
	}
	
	@Length(min=1, max=11, message="项目种类[1-储备项目，2-已投项目]长度必须介于 1 和 11 之间")
	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	
	@Length(min=0, max=11, message="项目来源[1-产业链，2-同行，3-已投项目介绍，4-政府/招商，5-券商/律所/会所，6-FA，7-主动发掘，8-其他]长度必须介于 0 和 11 之间")
	public String getProjectSource() {
		return projectSource;
	}

	public void setProjectSource(String projectSource) {
		this.projectSource = projectSource;
	}
	
	@Length(min=1, max=11, message="发展阶段[1-天使，2-Pre-A，3-A，4-B，5-Pre-IPO]长度必须介于 1 和 11 之间")
	public String getProjectStage() {
		return projectStage;
	}

	public void setProjectStage(String projectStage) {
		this.projectStage = projectStage;
	}
	
	@Length(min=0, max=64, message="省长度必须介于 0 和 64 之间")
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@Length(min=0, max=64, message="市长度必须介于 0 和 64 之间")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@Length(min=0, max=64, message="区/县长度必须介于 0 和 64 之间")
	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	@Length(min=1, max=256, message="地点长度必须介于 1 和 256 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Length(min=0, max=1200, message="项目简介长度必须介于 0 和 1200 之间")
	public String getProjectIntroduction() {
		return projectIntroduction;
	}

	public void setProjectIntroduction(String projectIntroduction) {
		this.projectIntroduction = projectIntroduction;
	}
	
	@Length(min=0, max=256, message="备注长度必须介于 0 和 256 之间")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getReserveInfoPfa() {
		return reserveInfoPfa;
	}

	public void setReserveInfoPfa(String reserveInfoPfa) {
		this.reserveInfoPfa = reserveInfoPfa;
	}
	
	public String getReserveInfoPfv() {
		return reserveInfoPfv;
	}

	public void setReserveInfoPfv(String reserveInfoPfv) {
		this.reserveInfoPfv = reserveInfoPfv;
	}
	
	@Length(min=0, max=20, message="已投项目信息-项目编号长度必须介于 0 和 20 之间")
	public String getInvestedInfoProjectNo() {
		return investedInfoProjectNo;
	}

	public void setInvestedInfoProjectNo(String investedInfoProjectNo) {
		this.investedInfoProjectNo = investedInfoProjectNo;
	}
	
	@Length(min=0, max=50, message="已投项目信息-法人代表长度必须介于 0 和 50 之间")
	public String getInvestedInfoLegalRepresentative() {
		return investedInfoLegalRepresentative;
	}

	public void setInvestedInfoLegalRepresentative(String investedInfoLegalRepresentative) {
		this.investedInfoLegalRepresentative = investedInfoLegalRepresentative;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getInvestedInfoFoundingTime() {
		return investedInfoFoundingTime;
	}

	public void setInvestedInfoFoundingTime(Date investedInfoFoundingTime) {
		this.investedInfoFoundingTime = investedInfoFoundingTime;
	}
	
	public String getInvestedInfoRegisteredCapital() {
		return investedInfoRegisteredCapital;
	}

	public void setInvestedInfoRegisteredCapital(String investedInfoRegisteredCapital) {
		this.investedInfoRegisteredCapital = investedInfoRegisteredCapital;
	}
	
	@Length(min=0, max=20, message="已投项目信息-统一信用代码长度必须介于 0 和 20 之间")
	public String getInvestedInfoUniformCreditCode() {
		return investedInfoUniformCreditCode;
	}

	public void setInvestedInfoUniformCreditCode(String investedInfoUniformCreditCode) {
		this.investedInfoUniformCreditCode = investedInfoUniformCreditCode;
	}
	
	@Length(min=0, max=20, message="已投项目信息-银行信息-户名长度必须介于 0 和 20 之间")
	public String getInvestedInfoAccountName() {
		return investedInfoAccountName;
	}

	public void setInvestedInfoAccountName(String investedInfoAccountName) {
		this.investedInfoAccountName = investedInfoAccountName;
	}
	
	@Length(min=0, max=50, message="已投项目信息-银行信息-开户行长度必须介于 0 和 50 之间")
	public String getInvestedInfoBankName() {
		return investedInfoBankName;
	}

	public void setInvestedInfoBankName(String investedInfoBankName) {
		this.investedInfoBankName = investedInfoBankName;
	}
	
	@Length(min=0, max=20, message="已投项目信息-银行信息-账号长度必须介于 0 和 20 之间")
	public String getInvestedInfoAccountNo() {
		return investedInfoAccountNo;
	}

	public void setInvestedInfoAccountNo(String investedInfoAccountNo) {
		this.investedInfoAccountNo = investedInfoAccountNo;
	}
	
	@Length(min=0, max=256, message="已投项目信息-银行信息-备注长度必须介于 0 和 256 之间")
	public String getInvestedInfoComment() {
		return investedInfoComment;
	}

	public void setInvestedInfoComment(String investedInfoComment) {
		this.investedInfoComment = investedInfoComment;
	}
	
	@Length(min=0, max=2000, message="二维码长度必须介于 0 和 2000 之间")
	public String getQrCodeFile() {
		return qrCodeFile;
	}

	public void setQrCodeFile(String qrCodeFile) {
		this.qrCodeFile = qrCodeFile;
	}
	
	@Length(min=0, max=2000, message="项目计划书长度必须介于 0 和 2000 之间")
	public String getProjectPlanFile() {
		return projectPlanFile;
	}

	public void setProjectPlanFile(String projectPlanFile) {
		this.projectPlanFile = projectPlanFile;
	}
	
	@Length(min=0, max=2000, message="保密协议/TS长度必须介于 0 和 2000 之间")
	public String getSecrecyFile() {
		return secrecyFile;
	}

	public void setSecrecyFile(String secrecyFile) {
		this.secrecyFile = secrecyFile;
	}
	
	@Length(min=0, max=2000, message="Logo长度必须介于 0 和 2000 之间")
	public String getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(String logoFile) {
		this.logoFile = logoFile;
	}
	
	@Length(min=0, max=2000, message="营业执照长度必须介于 0 和 2000 之间")
	public String getBusinessLicenseFile() {
		return businessLicenseFile;
	}

	public void setBusinessLicenseFile(String businessLicenseFile) {
		this.businessLicenseFile = businessLicenseFile;
	}
	
	@Length(min=0, max=256, message="备注（行业分类为9-其他时的备注）长度必须介于 0 和 256 之间")
	public String getIndustryComment() {
		return industryComment;
	}

	public void setIndustryComment(String industryComment) {
		this.industryComment = industryComment;
	}
	
	@Length(min=1, max=11, message="数据版本号[默认为1]长度必须介于 1 和 11 之间")
	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	
	public List<TblProjectInvestInfo> getTblProjectInvestInfoList() {
		return tblProjectInvestInfoList;
	}

	public void setTblProjectInvestInfoList(List<TblProjectInvestInfo> tblProjectInvestInfoList) {
		this.tblProjectInvestInfoList = tblProjectInvestInfoList;
	}
}