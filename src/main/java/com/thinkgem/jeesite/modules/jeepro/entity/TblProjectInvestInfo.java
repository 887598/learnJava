/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jeepro.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 项目一对多Entity
 * @author xuezp
 * @version 2017-08-16
 */
public class TblProjectInvestInfo extends DataEntity<TblProjectInvestInfo> {
	
	private static final long serialVersionUID = 1L;
	private TblProjectInfo projectInfoId;		// 项目信息表ID 父类
	private String vcProjectNo;		// 基金项目编号
	private Date initialTime;		// 立项时间
	private Date decisionTime;		// 决策时间
	private String currentFinancingAmount;		// 本轮融资金额
	private String beforeInvestValuation;		// 投前估值
	private String investPercent;		// 投资占比
	private String leadInvestFlg;		// 是否领投[0-否；1-是]
	private String investProperty;		// 投资性质[1-增资；2-转让；3-可转债；4-代持]
	private String mcInfoId;		// 管理公司信息表ID
	private String vcInfoId;		// 基金信息表ID
	private String contributionType;		// 出资方式[1-一次性出资；2-分批出资；3-可转债借款]
	private String comment;		// 备注
	private String versionNo;		// 数据版本号[默认为1]
	
	public TblProjectInvestInfo() {
		super();
	}

	public TblProjectInvestInfo(String id){
		super(id);
	}

	public TblProjectInvestInfo(TblProjectInfo projectInfoId){
		this.projectInfoId = projectInfoId;
	}

	@Length(min=1, max=11, message="项目信息表ID长度必须介于 1 和 11 之间")
	public TblProjectInfo getProjectInfoId() {
		return projectInfoId;
	}

	public void setProjectInfoId(TblProjectInfo projectInfoId) {
		this.projectInfoId = projectInfoId;
	}
	
	@Length(min=1, max=20, message="基金项目编号长度必须介于 1 和 20 之间")
	public String getVcProjectNo() {
		return vcProjectNo;
	}

	public void setVcProjectNo(String vcProjectNo) {
		this.vcProjectNo = vcProjectNo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getInitialTime() {
		return initialTime;
	}

	public void setInitialTime(Date initialTime) {
		this.initialTime = initialTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDecisionTime() {
		return decisionTime;
	}

	public void setDecisionTime(Date decisionTime) {
		this.decisionTime = decisionTime;
	}
	
	public String getCurrentFinancingAmount() {
		return currentFinancingAmount;
	}

	public void setCurrentFinancingAmount(String currentFinancingAmount) {
		this.currentFinancingAmount = currentFinancingAmount;
	}
	
	public String getBeforeInvestValuation() {
		return beforeInvestValuation;
	}

	public void setBeforeInvestValuation(String beforeInvestValuation) {
		this.beforeInvestValuation = beforeInvestValuation;
	}
	
	public String getInvestPercent() {
		return investPercent;
	}

	public void setInvestPercent(String investPercent) {
		this.investPercent = investPercent;
	}
	
	@Length(min=1, max=11, message="是否领投[0-否；1-是]长度必须介于 1 和 11 之间")
	public String getLeadInvestFlg() {
		return leadInvestFlg;
	}

	public void setLeadInvestFlg(String leadInvestFlg) {
		this.leadInvestFlg = leadInvestFlg;
	}
	
	@Length(min=1, max=11, message="投资性质[1-增资；2-转让；3-可转债；4-代持]长度必须介于 1 和 11 之间")
	public String getInvestProperty() {
		return investProperty;
	}

	public void setInvestProperty(String investProperty) {
		this.investProperty = investProperty;
	}
	
	@Length(min=0, max=11, message="管理公司信息表ID长度必须介于 0 和 11 之间")
	public String getMcInfoId() {
		return mcInfoId;
	}

	public void setMcInfoId(String mcInfoId) {
		this.mcInfoId = mcInfoId;
	}
	
	@Length(min=0, max=11, message="基金信息表ID长度必须介于 0 和 11 之间")
	public String getVcInfoId() {
		return vcInfoId;
	}

	public void setVcInfoId(String vcInfoId) {
		this.vcInfoId = vcInfoId;
	}
	
	@Length(min=0, max=11, message="出资方式[1-一次性出资；2-分批出资；3-可转债借款]长度必须介于 0 和 11 之间")
	public String getContributionType() {
		return contributionType;
	}

	public void setContributionType(String contributionType) {
		this.contributionType = contributionType;
	}
	
	@Length(min=0, max=256, message="备注长度必须介于 0 和 256 之间")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	@Length(min=1, max=11, message="数据版本号[默认为1]长度必须介于 1 和 11 之间")
	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}
	
}