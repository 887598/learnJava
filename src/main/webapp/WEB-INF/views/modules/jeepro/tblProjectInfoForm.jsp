<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>项目一对多管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/jeepro/tblProjectInfo/">项目一对多列表</a></li>
		<li class="active"><a href="${ctx}/jeepro/tblProjectInfo/form?id=${tblProjectInfo.id}">项目一对多<shiro:hasPermission name="jeepro:tblProjectInfo:edit">${not empty tblProjectInfo.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="jeepro:tblProjectInfo:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="tblProjectInfo" action="${ctx}/jeepro/tblProjectInfo/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">企业名称：</label>
			<div class="controls">
				<form:input path="enterpriseName" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">企业简称：</label>
			<div class="controls">
				<form:input path="enterpriseAbbreviation" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">项目种类[1-储备项目，2-已投项目]：</label>
			<div class="controls">
				<form:input path="projectType" htmlEscape="false" maxlength="11" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">项目来源[1-产业链，2-同行，3-已投项目介绍，4-政府/招商，5-券商/律所/会所，6-FA，7-主动发掘，8-其他]：</label>
			<div class="controls">
				<form:input path="projectSource" htmlEscape="false" maxlength="11" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发展阶段[1-天使，2-Pre-A，3-A，4-B，5-Pre-IPO]：</label>
			<div class="controls">
				<form:input path="projectStage" htmlEscape="false" maxlength="11" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">省：</label>
			<div class="controls">
				<form:input path="province" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">市：</label>
			<div class="controls">
				<form:input path="city" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">区/县：</label>
			<div class="controls">
				<form:input path="county" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">地点：</label>
			<div class="controls">
				<form:input path="address" htmlEscape="false" maxlength="256" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">项目简介：</label>
			<div class="controls">
				<form:input path="projectIntroduction" htmlEscape="false" maxlength="1200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				<form:input path="comment" htmlEscape="false" maxlength="256" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">储备项目信息-拟融资金额：</label>
			<div class="controls">
				<form:input path="reserveInfoPfa" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">储备项目信息-拟融资估值：</label>
			<div class="controls">
				<form:input path="reserveInfoPfv" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-项目编号：</label>
			<div class="controls">
				<form:input path="investedInfoProjectNo" htmlEscape="false" maxlength="20" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-法人代表：</label>
			<div class="controls">
				<form:input path="investedInfoLegalRepresentative" htmlEscape="false" maxlength="50" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-成立时间：</label>
			<div class="controls">
				<input name="investedInfoFoundingTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${tblProjectInfo.investedInfoFoundingTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-注册资本：</label>
			<div class="controls">
				<form:input path="investedInfoRegisteredCapital" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-统一信用代码：</label>
			<div class="controls">
				<form:input path="investedInfoUniformCreditCode" htmlEscape="false" maxlength="20" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-银行信息-户名：</label>
			<div class="controls">
				<form:input path="investedInfoAccountName" htmlEscape="false" maxlength="20" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-银行信息-开户行：</label>
			<div class="controls">
				<form:input path="investedInfoBankName" htmlEscape="false" maxlength="50" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-银行信息-账号：</label>
			<div class="controls">
				<form:input path="investedInfoAccountNo" htmlEscape="false" maxlength="20" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">已投项目信息-银行信息-备注：</label>
			<div class="controls">
				<form:input path="investedInfoComment" htmlEscape="false" maxlength="256" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">二维码：</label>
			<div class="controls">
				<form:input path="qrCodeFile" htmlEscape="false" maxlength="2000" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">项目计划书：</label>
			<div class="controls">
				<form:input path="projectPlanFile" htmlEscape="false" maxlength="2000" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">保密协议/TS：</label>
			<div class="controls">
				<form:input path="secrecyFile" htmlEscape="false" maxlength="2000" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Logo：</label>
			<div class="controls">
				<form:input path="logoFile" htmlEscape="false" maxlength="2000" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">营业执照：</label>
			<div class="controls">
				<form:input path="businessLicenseFile" htmlEscape="false" maxlength="2000" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注（行业分类为9-其他时的备注）：</label>
			<div class="controls">
				<form:input path="industryComment" htmlEscape="false" maxlength="256" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">数据版本号[默认为1]：</label>
			<div class="controls">
				<form:input path="versionNo" htmlEscape="false" maxlength="11" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="1200" class="input-xxlarge "/>
			</div>
		</div>
			<div class="control-group">
				<label class="control-label">tbl_project_invest_info：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>基金项目编号</th>
								<th>立项时间</th>
								<th>决策时间</th>
								<th>本轮融资金额</th>
								<th>投前估值</th>
								<th>投资占比</th>
								<th>是否领投[0-否；1-是]</th>
								<th>投资性质[1-增资；2-转让；3-可转债；4-代持]</th>
								<th>管理公司信息表ID</th>
								<th>基金信息表ID</th>
								<th>出资方式[1-一次性出资；2-分批出资；3-可转债借款]</th>
								<th>备注</th>
								<th>数据版本号[默认为1]</th>
								<th>备注信息</th>
								<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="tblProjectInvestInfoList">
						</tbody>
						<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><tfoot>
							<tr><td colspan="16"><a href="javascript:" onclick="addRow('#tblProjectInvestInfoList', tblProjectInvestInfoRowIdx, tblProjectInvestInfoTpl);tblProjectInvestInfoRowIdx = tblProjectInvestInfoRowIdx + 1;" class="btn">新增</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					<script type="text/template" id="tblProjectInvestInfoTpl">//<!--
						<tr id="tblProjectInvestInfoList{{idx}}">
							<td class="hide">
								<input id="tblProjectInvestInfoList{{idx}}_id" name="tblProjectInvestInfoList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="tblProjectInvestInfoList{{idx}}_delFlag" name="tblProjectInvestInfoList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_vcProjectNo" name="tblProjectInvestInfoList[{{idx}}].vcProjectNo" type="text" value="{{row.vcProjectNo}}" maxlength="20" class="input-small required"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_initialTime" name="tblProjectInvestInfoList[{{idx}}].initialTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
									value="{{row.initialTime}}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_decisionTime" name="tblProjectInvestInfoList[{{idx}}].decisionTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
									value="{{row.decisionTime}}" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_currentFinancingAmount" name="tblProjectInvestInfoList[{{idx}}].currentFinancingAmount" type="text" value="{{row.currentFinancingAmount}}" class="input-small required"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_beforeInvestValuation" name="tblProjectInvestInfoList[{{idx}}].beforeInvestValuation" type="text" value="{{row.beforeInvestValuation}}" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_investPercent" name="tblProjectInvestInfoList[{{idx}}].investPercent" type="text" value="{{row.investPercent}}" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_leadInvestFlg" name="tblProjectInvestInfoList[{{idx}}].leadInvestFlg" type="text" value="{{row.leadInvestFlg}}" maxlength="11" class="input-small required"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_investProperty" name="tblProjectInvestInfoList[{{idx}}].investProperty" type="text" value="{{row.investProperty}}" maxlength="11" class="input-small required"/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_mcInfoId" name="tblProjectInvestInfoList[{{idx}}].mcInfoId" type="text" value="{{row.mcInfoId}}" maxlength="11" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_vcInfoId" name="tblProjectInvestInfoList[{{idx}}].vcInfoId" type="text" value="{{row.vcInfoId}}" maxlength="11" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_contributionType" name="tblProjectInvestInfoList[{{idx}}].contributionType" type="text" value="{{row.contributionType}}" maxlength="11" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_comment" name="tblProjectInvestInfoList[{{idx}}].comment" type="text" value="{{row.comment}}" maxlength="256" class="input-small "/>
							</td>
							<td>
								<input id="tblProjectInvestInfoList{{idx}}_versionNo" name="tblProjectInvestInfoList[{{idx}}].versionNo" type="text" value="{{row.versionNo}}" maxlength="11" class="input-small required"/>
							</td>
							<td>
								<textarea id="tblProjectInvestInfoList{{idx}}_remarks" name="tblProjectInvestInfoList[{{idx}}].remarks" rows="4" maxlength="1200" class="input-small ">{{row.remarks}}</textarea>
							</td>
							<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#tblProjectInvestInfoList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>//-->
					</script>
					<script type="text/javascript">
						var tblProjectInvestInfoRowIdx = 0, tblProjectInvestInfoTpl = $("#tblProjectInvestInfoTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(tblProjectInfo.tblProjectInvestInfoList)};
							for (var i=0; i<data.length; i++){
								addRow('#tblProjectInvestInfoList', tblProjectInvestInfoRowIdx, tblProjectInvestInfoTpl, data[i]);
								tblProjectInvestInfoRowIdx = tblProjectInvestInfoRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="jeepro:tblProjectInfo:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>