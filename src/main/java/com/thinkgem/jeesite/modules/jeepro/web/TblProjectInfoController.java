/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.jeepro.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.jeepro.entity.TblProjectInfo;
import com.thinkgem.jeesite.modules.jeepro.service.TblProjectInfoService;

/**
 * 项目一对多Controller
 * @author xuezp
 * @version 2017-08-16
 */
@Controller
@RequestMapping(value = "${adminPath}/jeepro/tblProjectInfo")
public class TblProjectInfoController extends BaseController {

	@Autowired
	private TblProjectInfoService tblProjectInfoService;
	
	@ModelAttribute
	public TblProjectInfo get(@RequestParam(required=false) String id) {
		TblProjectInfo entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tblProjectInfoService.get(id);
		}
		if (entity == null){
			entity = new TblProjectInfo();
		}
		return entity;
	}
	
	@RequiresPermissions("jeepro:tblProjectInfo:view")
	@RequestMapping(value = {"list", ""})
	public String list(TblProjectInfo tblProjectInfo, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TblProjectInfo> page = tblProjectInfoService.findPage(new Page<TblProjectInfo>(request, response), tblProjectInfo); 
		model.addAttribute("page", page);
		return "modules/jeepro/tblProjectInfoList";
	}

	@RequiresPermissions("jeepro:tblProjectInfo:view")
	@RequestMapping(value = "form")
	public String form(TblProjectInfo tblProjectInfo, Model model) {
		model.addAttribute("tblProjectInfo", tblProjectInfo);
		return "modules/jeepro/tblProjectInfoForm";
	}

	@RequiresPermissions("jeepro:tblProjectInfo:edit")
	@RequestMapping(value = "save")
	public String save(TblProjectInfo tblProjectInfo, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, tblProjectInfo)){
			return form(tblProjectInfo, model);
		}
		tblProjectInfoService.save(tblProjectInfo);
		addMessage(redirectAttributes, "保存项目一对多成功");
		return "redirect:"+Global.getAdminPath()+"/jeepro/tblProjectInfo/?repage";
	}
	
	@RequiresPermissions("jeepro:tblProjectInfo:edit")
	@RequestMapping(value = "delete")
	public String delete(TblProjectInfo tblProjectInfo, RedirectAttributes redirectAttributes) {
		tblProjectInfoService.delete(tblProjectInfo);
		addMessage(redirectAttributes, "删除项目一对多成功");
		return "redirect:"+Global.getAdminPath()+"/jeepro/tblProjectInfo/?repage";
	}

}